import { Injectable } from '@angular/core';
import * as $ from 'jquery'
@Injectable({
  providedIn: 'root'
})
export class AccessibleService {
  
  public isAccessible = false

  constructor() {
    this.toggleAccessible(false);
  }

 


  toggleAccessible(defaultChoice = true) {
    if (!this.isAccessible && defaultChoice) {
      this.isAccessible = true;
      this.enterAccessMode(screen.height / 2.5);
    }
    else {
      this.isAccessible = false;
      this.enterAccessMode(0);
    }
  }
  enterAccessMode(num): any {
    var content = $('.access');
    if (content.length>0) {
      content.animate({ top: num }, 500);
    }
  }
    
  
}
