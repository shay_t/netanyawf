import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as $ from 'jquery';
import { ServService } from './serv.service';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiWeatherService {

  public weather;
  public icon;
  public temp;
  public url;
  constructor(private http: HttpClient, public serv: ServService) {
    this.url = this.serv.getMetaValue("weatherUrl");
    this.getWeather()
  }

  


  async getWeather() {

    await $.post(`${this.url}/Weather/GetWeatherBySignId`, { signId: this.serv.signId }).done((data) => {
      this.weather = data;
      if (this.weather[0] != null) {
        this.icon = this.weather[0].Icon;
        this.temp = this.weather[0].Temp;
      }
    });

    if (this.weather[0] == null) {

      await this.http.get('https://api.apixu.com/v1/forecast.json?key=96f33757a608475e903140117182511&q=netanya')
        .pipe(map(res => res))
        .subscribe(res => {
          this.weather = res;
          this.temp = this.weather.current.temp_c;
          this.icon = this.weather.current.condition.icon;
          console.log(this.weather);

          var weatherToDB = [{
            Temp: this.temp,
            Icon: this.icon,
            SignId: this.serv.signId,
            Date: this.weather.forecast.forecastday[0].date
          }];
          $.post(`${this.url}/Weather/SetWeather/`, {
            Items: weatherToDB
          });
        });
    }
  }
}





