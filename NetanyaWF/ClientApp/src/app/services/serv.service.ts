import { Injectable,ViewChild } from '@angular/core';
@Injectable()
export class ServService {
  currentLang = 'he';
  initHome = 0;
  initBaseHome = 120;
  public isWF = true;
  signId: any;
  constructor() {
    var url = new URL(location.href);
    this.signId = url.searchParams.get("signId");
    setInterval(() => {
      this.refreshHome();
    }, 1000);
  }

  refreshHome() {
    this.initHome++;
    if (this.initHome == this.initBaseHome) {
      this.isWF = true;
      (<HTMLIFrameElement>document.getElementById('wfIframe')).contentWindow.postMessage('redirect-home', '*');
      (<HTMLIFrameElement>document.getElementById('iframe')).contentWindow.postMessage('redirect-home', '*');
      this.initHome = 0;
    }
  }

  getMetaValue(name) {
    return document.querySelector(`meta[name=${name}]`).getAttribute('value')
  }

}
