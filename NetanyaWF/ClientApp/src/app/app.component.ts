import { Component, HostListener, ViewChild, ElementRef } from '@angular/core';
import { ServService } from './services/serv.service';
import { ApiWeatherService } from './services/api-weather.service';
import { AccessibleService } from './services/accessible.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  time: string;
  date: string;
  @ViewChild('wfIframe') wfIframe: ElementRef<HTMLIFrameElement>;
  @ViewChild('iframe') iframe: ElementRef<HTMLIFrameElement>;

  constructor(public serv: ServService, public weather: ApiWeatherService, private access: AccessibleService) {
    window.addEventListener("message", (ev) => { this.onMessage(ev); }, false);
    document.onkeydown=(ev) => {
      if (ev.key == 'Tab')
        this.showCursor()
    };
    setInterval(() => {
      var d = new Date;

      var h = d.getHours();
      var m = d.getMinutes();

      this.time = `${this.getDD(h)}:${this.getDD(m)}`
      this.date = `${d.getDate()}.${d.getMonth() + 1}.${d.getFullYear()}`
    }, 1000);
  };

  onMessage(ev) {
    this.serv.initHome = 0;
    if (ev.data != null) {
      switch (ev.data) {
        case 'redirect-home':
          this.access.toggleAccessible();
          this.serv.isWF = true;
          this.wfIframe.nativeElement.contentWindow.postMessage('redirect-home', '*');
          break;
      }
    }
  }

  @HostListener('click', ['$event'])
  touchEvent(ev) {
    this.serv.initHome = 0;
  }

  showCursor() {
    var elements = <any>document.body.querySelectorAll('*');
    elements.forEach((e: any) => {
      e.style.cursor = "auto";
    });
  }

  getDD(num) {
    return (num >= 10) ? num : "0" + num;
  }

  menuAction(action) {
    if (action == 'accessibility') {
      this.access.toggleAccessible();
      return;
    }
    if (action == 'redirect-home') {
      this.access.toggleAccessible(false);
    }
    this.wfIframe.nativeElement.contentWindow.postMessage(action, '*');
    this.iframe.nativeElement.contentWindow.postMessage(action, '*');

  }

  clickedLogo() {
    window.parent.postMessage({ 'logoClicked': true, 'project': 'wip' }, '*');
  }
}
