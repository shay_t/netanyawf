import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { UrlSafePipe } from './pipes/urlSafe.pipe';
import { TranslatePipe } from './pipes/translate.pipe';
import { TranslateService } from './services/translate.service';
import { ServService } from './services/serv.service';
import { ApiWeatherService } from './services/api-weather.service';

export function setupTranslateFactory(
  service: TranslateService): Function {
  return () => service.use('he');
}

@NgModule({
  declarations: [
    AppComponent,
    UrlSafePipe,
    TranslatePipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule
  ],
  providers: [TranslateService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [TranslateService],
      multi: true
    }, ServService, ApiWeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
